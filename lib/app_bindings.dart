import 'package:get/get.dart';
import 'package:routelines/controllers/mainController.dart';

import 'controllers/controllController.dart';
import 'controllers/mapController.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(MainController());
    Get.put(ControllController());
    Get.put(MapController());
  }
}
