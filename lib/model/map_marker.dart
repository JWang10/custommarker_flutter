import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Marker {
  final Map<MarkerId, Marker> markers;
  final int? countPins;
  final int? sumPins;

  Marker({
    required this.markers,
    this.countPins,
    this.sumPins,
  }) : assert(markers != null);

  // int get countPins => countPins;

}
