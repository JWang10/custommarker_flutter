import 'package:flutter/material.dart';

class HelperFunction {
  static Future<void> showCloseAlertDialog({
    required BuildContext context,
    required Function onPressed,
  }) async {
    return showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.warning,
              size: 36.0,
            ),
            SizedBox(width: 5.0),
            Text(
              "Warning",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        content: new Text(
          "Are you sure to do this?",
          style: TextStyle(fontSize: 20.0),
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green[500],
                  ),
                  child: Text('Yes'),
                  onPressed: onPressed as void Function()?,
                ),
                SizedBox(
                  width: 10,
                ),
                ElevatedButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static Future<void> showAlertDialog({
    required BuildContext context,
    required String message,
  }) async {
    return showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.warning,
              size: 36.0,
            ),
            SizedBox(width: 5.0),
            Text(
              "Warning",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        content: new Text(
          message,
          style: TextStyle(fontSize: 20.0),
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
