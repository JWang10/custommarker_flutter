class Car {
  String? battery;
  String? id;
  Position? position;
  String? taskstatus;
  String? time;

  Car({this.battery, this.id, this.position, this.taskstatus, this.time});

  Car.fromJson(Map<String, dynamic> json) {
    battery = json['battery'];
    id = json['id'];
    position =
        json['position'] != null ? Position?.fromJson(json['position']) : null;
    taskstatus = json['taskstatus'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['battery'] = battery;
    data['id'] = id;
    if (position != null) {
      data['position'] = position!.toJson();
    }
    data['taskstatus'] = taskstatus;
    data['time'] = time;
    return data;
  }
}

class ToCar {
  String? id;
  Position? position;

  ToCar({this.id, this.position});

  ToCar.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    position =
        json['position'] != null ? Position?.fromJson(json['position']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    if (position != null) {
      data['position'] = position!.toJson();
    }
    return data;
  }
}

class Position {
  String? latitude;
  String? longitude;

  Position({this.latitude, this.longitude});

  Position.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    return data;
  }

  @override
  String toString() {
    return "location: ${latitude!}, ${longitude!}";
  }
}
