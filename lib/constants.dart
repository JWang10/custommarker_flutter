class Constant {
  /// main page
  static List<String> mainConnectionStatus = ["no connection..", "connection"];
  static String maintitle = "CAR Controll";
  static String mainButtonController = "Controller";
  static String mainButtonNavigation = "Navigation";
  static String mainConnectionButton = "Connect";

  /// controller page
  /// map page
}
