import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:get/get.dart';

class ControllController extends GetxController
    with SingleGetTickerProviderMixin {
  AnimationController? _animationController;

  /// The alignment of the card as it is dragged or being animated.
  ///
  /// While the card is being dragged, this value is set to the values computed
  /// in the GestureDetector onPanUpdate callback. If the animation is running,
  /// this value is set to the value of the [_animation].
  late Animation<Alignment> _animation;
  var _dragAlignment = Alignment.center.obs;
  var _mode = "control".obs;
  String getMode() => _mode.value;
  Alignment get dragAlignment => _dragAlignment.value;
  AnimationController? get animationController => _animationController;

  @override
  Future<void> onInit() async {
    super.onInit();
    initAnimationController();
  }

  @override
  void onClose() {
    super.onClose();
    _animationController!.dispose();
  }

  initAnimationController() {
    _animationController = AnimationController(vsync: this);
    _animationController!.addListener(() {
      _dragAlignment.value = _animation.value;
    });
    // update();
  }

  void setDragAlignment(DragUpdateDetails details, Size size) {
    _dragAlignment.value += Alignment(
      details.delta.dx / (size.width / 1.1),
      details.delta.dy / (size.height / 1.1),
    );
    print(_dragAlignment.value);

    /// 255 - 112 - 0 (forwrad - stop - backward)
    double delta = 0.0;
    double realValue = 0.0;
    if (_dragAlignment.value.y > 0) {
      delta = (255 - 122) / (0 - _dragAlignment.value.y);
      realValue = _dragAlignment.value.y * delta;
    } else {
      delta = (122 - 0) / (_dragAlignment.value.y - 0);
      realValue = _dragAlignment.value.y * delta;
    }

    // X - 112 =
    realValue = _dragAlignment.value.y * delta;
    print(_dragAlignment.value.y);
    print(delta);
    print(realValue);
  }

  void setMode(String mode) {
    this._mode.value = mode;
  }

  /// Calculates and runs a [s].
  void runAnimation(Offset pixelsPerSecond, Size size) {
    _animation = _animationController!.drive(
      AlignmentTween(
        begin: _dragAlignment.value,
        end: Alignment.center,
      ),
    );
    // Calculate the velocity relative to the unit interval, [0,1],
    // used by the animation controller.
    final unitsPerSecondX = pixelsPerSecond.dx / size.width;
    final unitsPerSecondY = pixelsPerSecond.dy / size.height;
    final unitsPerSecond = Offset(unitsPerSecondX, unitsPerSecondY);
    final unitVelocity = unitsPerSecond.distance;

    const spring = SpringDescription(
      mass: 30,
      stiffness: 1,
      damping: 1,
    );

    final simulation = SpringSimulation(spring, 0, 1, -unitVelocity);

    _animationController!.animateWith(simulation);
    // update();
  }
}
