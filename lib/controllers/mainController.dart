import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class MainController extends GetxController {
  late TextEditingController _tfController;
  var _tfValidate = false.obs;
  var _connectionStatus = false.obs;
  bool get connectionStatus => _connectionStatus.value;
  bool get tfValidate => _tfValidate.value;
  bool get tfIsEmpty => _tfController.text.isEmpty;
  TextEditingController get tfController => _tfController;

  @override
  void onInit() {
    super.onInit();
    _tfController = TextEditingController();
  }

  set connectionStatus(bool status) {
    _connectionStatus.value = status;
  }

  set tfValidate(bool validate) {
    _tfValidate.value = validate;
  }
}
