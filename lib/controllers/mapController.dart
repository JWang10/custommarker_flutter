import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:routelines/components/http_service.dart';
import 'package:routelines/model/car_model.dart';

import 'mainController.dart';

class MapController extends GetxController {
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> _polylineCoordinates = [];
  var _countPins = 0.obs;
  var _navStatus = false.obs;
  var _dropdownValue = "".obs; // like independent provider
  late Car _carStatus;
  int _carCheckCounter = 0;

  List<LatLng> get polylineCoordinates => _polylineCoordinates;
  Map<MarkerId, Marker>? getMarkers() => markers;
  Map<PolylineId, Polyline>? getPolylines() => polylines;
  int getCountPins() => _countPins.value;
  bool getNavStatus() => _navStatus.value;
  String getDropDownValue() => _dropdownValue.value;
  Car get carStatus => _carStatus;

  @override
  Future<void> onInit() async {
    super.onInit();
    // updateCARdata();
  }

  cancel() {
    this._countPins.value = 0;
  }

  void setMarkers(Map<MarkerId, Marker> markers) {
    this.markers = markers;
    update();
  }

  void setPolylines(Map<PolylineId, Polyline> polylines) {
    this.polylines = polylines;
    update();
  }

  void updateCountPin() {
    this._countPins++;
    // this._sumPins.value++;
  }

  void setNavStatus(bool navStatus) {
    this._navStatus.value = navStatus;
    if (!this._navStatus.value) {
      this.polylines.clear();
    }
    update();
  }

  void setDropDownValue(String value) {
    this._dropdownValue.value = value;
  }

  void removeMarker(MarkerId markerId) {
    this.markers.remove(markerId);
    update();
  }

  void removeAllMarker(Map<MarkerId, Marker> markers) {
    this._countPins.value = 0;
    this.markers = markers;
    this.markers.removeWhere((key, value) => key.value != "CAR");
    // this.markers.clear();
    this.polylines.clear();
    update();
  }

  // onLongPress on map to setState()
  void addMarker(Map<MarkerId, Marker> markers) {
    this.markers = markers;
    update();
  }

  void addPolyline() async {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.red,
      points: polylineCoordinates,
      // points: Provider.of<AppState>(context, listen: false).getPolylines(),
    );
    polylines[id] = polyline;
    setPolylines(polylines);
    update();
  }

  void updateMarker(
    MarkerId markerId,
    String name,
  ) {
    Marker now = this.markers[markerId]!;
    Marker updatedMarker = now.copyWith(
      infoWindowParam: InfoWindow(
        title: name,
        snippet: now.infoWindow.snippet,
        onTap: now.infoWindow.onTap,
      ),
    );
    this.markers.remove(now);
    this.markers[markerId] = updatedMarker;
    update();
  }

  void updateCARdata() async {
    try {
      var res = await API.getCarData(
          url: Get.find<MainController>().tfController.text);
      Car carJson = Car.fromJson(jsonDecode(res));
      // print("${carJson.position!.latitude} ${carJson.position!.longitude}");
      this._carStatus = carJson;

      /// navigated successfully
      /// whether be with ? fps to check STATUS ?
      if (_carCheckCounter > 5 &&
          this._carStatus.taskstatus!.toLowerCase() != "active") {
        this.setNavStatus(false);
        _carCheckCounter = 0;

        /// confirm CAR to cancel?
      }
      if (this.getNavStatus()) _carCheckCounter++;
    } catch (e) {
      _carCheckCounter = 0;
      printError(info: "no car's connection");
      // throw Exception("no connection");
      Get.snackbar("Error", "no car's connection",
          duration: Duration(seconds: 2));
    }
  }

  void sendTargetToCAR() async {
    var target = getDropDownValue();
    if (target != "") {
      // Start Navigatoring to target
      // Draw a line from current location to target
      // and show real-time current marker
      polylineCoordinates.clear();
      polylineCoordinates.add(LatLng(
          double.parse(_carStatus.position!.latitude!),
          double.parse(_carStatus.position!.longitude!)));
      //Draw line
      var targetPoint =
          markers.values.where((key) => key.infoWindow.title == target);
      polylineCoordinates.add(targetPoint.first.position);
      // print(_carStatus.position!.toJson());
      // print(targetPoint.first.position);
      addPolyline();
      setNavStatus(true);
    }

    ToCar carJson = ToCar(
      id: "Geosat-UGV",
      position: Position(
          latitude: polylineCoordinates.elementAt(1).latitude.toString(),
          longitude: polylineCoordinates.elementAt(1).longitude.toString()),
    );
    // printInfo(info: carJson.toJson().toString());
    var res = await API.send(
        url: Get.find<MainController>().tfController.text, data: carJson);
    // printInfo(info: res);
  }
}
