import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:routelines/app_bindings.dart';
import 'package:routelines/views/main_page.dart';
import 'views/control_page.dart';
import 'views/map_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Car Controller',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: ChangeNotifierProvider(
      //   create: (BuildContext context) => ControlAppState(),
      //   child: ControlPage(),
      // ),
      // home: MainPage(),
      getPages: [
        GetPage(
            name: Routes.HOME, page: () => MainPage(), binding: AppBindings()),
        GetPage(
          name: Routes.CONTROLLER,
          page: () => ControlPage(),
          binding: AppBindings(),
        ),
        GetPage(
          name: Routes.MAP,
          page: () => MapPage(),
          binding: AppBindings(),
        ),
      ],
      initialRoute: Routes.HOME,
    );
  }
}

abstract class Routes {
  // static const INITIAL = '/';
  static const HOME = '/home';
  static const CONTROLLER = '/controller';
  static const MAP = '/map';
}
