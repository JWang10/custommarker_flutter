import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:routelines/controllers/mapController.dart';

class DropDownList extends StatefulWidget {
  const DropDownList({Key? key, required this.markers}) : super(key: key);
  final Map<MarkerId, Marker> markers;

  @override
  _DropDownListState createState() => _DropDownListState();
}

class _DropDownListState extends State<DropDownList>
    with SingleTickerProviderStateMixin {
  // AnimationController _controller;
  String? dropdownValue;
  List<Marker> list = [];

  // @override
  // void initState() {
  //   super.initState();
  //   _controller = AnimationController(vsync: this);
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  //   _controller.dispose();
  // }
  var controller = Get.find<MapController>();
  @override
  Widget build(BuildContext context) {
    widget.markers.remove(MarkerId("CAR"));
    list = widget.markers.values.toList();
    controller.setDropDownValue(list.first.infoWindow.title.toString());
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(
        children: [
          Obx(
            () {
              return DropdownButton<String>(
                value: controller.getDropDownValue(),
                icon: Icon(Icons.keyboard_arrow_down),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String? newValue) {
                  controller.setDropDownValue(newValue!);
                },
                items: list.map<DropdownMenuItem<String>>((value) {
                  return DropdownMenuItem<String>(
                    value: value.infoWindow.title,
                    child: Text(value.infoWindow.title!),
                  );
                }).toList(),
              );
            },
          ),
        ],
      ),
    );
  }
}
