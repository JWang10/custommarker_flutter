import 'dart:convert';

import 'package:http/http.dart';
import 'package:routelines/model/car_model.dart';

class API {
  static Uri uri = Uri.http("127.0.0.1:5000", "/Test/RTK/GetRTKLocation");
  static Future<String> getGet() async {
    Response res = await get(uri);
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return "Error: " + res.body;
    }
  }

  // static Future<String> getCarData(String url) async {
  //   Uri? uri;

  //   /// e.g http://localhost:5000/Test/RTK/GetRTKLocation
  //   /// result: [http:,,localhost:5000,Test,RTK,GetRTKLocation] //if skip second element?
  //   List<String> urls = url.split('/');
  //   print(urls.join(","));
  //   String path = "";
  //   for (var i = 3; i < urls.length; i++) {
  //     path = path + "/" + urls[i];
  //   }
  //   print(path);
  //   switch (urls[0].startsWith("https")) {
  //     case true:
  //       uri = Uri.https(urls[2], path);
  //       break;
  //     case false:
  //       uri = Uri.http(urls[2], path);
  //       break;
  //   }
  //   // return uri.toString();
  //   Response res = await get(uri!);
  //   if (res.statusCode == 200) {
  //     return res.body;
  //   } else {
  //     return "Error: " + res.body;
  //   }
  // }
  // Future<String> getPost() async {
  //   uri = Uri.http("127.0.0.1:5000", "/Test/Uas/SetUASPositionlog");
  //   double x = 3.0;
  //   double y = 4.0;
  //   String jsonString = "[{\"x\":\"" +
  //       x.toString() +
  //       "\",\"y\":\"" +
  //       y.toString() +
  //       "\",\"id\":\"robot\"}]";
  //   Response res = await post(
  //     uri,
  //     body: jsonString,
  //     headers: {"Content-Type": "application/json"},
  //   );
  //   if (res.statusCode == 200) {
  //     return res.body;
  //   } else {
  //     return "Error: " + res.body;
  //   }
  // }
  static Future<String> getCarData({required String url}) async {
    Uri? uri;

    /// e.g http://localhost:5000/Test/RTK/GetRTKLocation
    /// result: [http:,,localhost:5000,Test,RTK,GetRTKLocation] //if skip second element?
    List<String> urls = url.split('/');
    // print(urls.join(","));
    String path = "";
    for (var i = 3; i < urls.length; i++) {
      path = path + "/" + urls[i];
    }
    // print(path);
    switch (urls[0].startsWith("https")) {
      case true:
        uri = Uri.https(urls[2], path);
        break;
      case false:
        uri = Uri.http(urls[2], path);
        break;
    }
    // return uri.toString();
    Response res = await get(uri!);
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return "Error: " + res.body;
    }
  }

  /// send data to CAR
  static Future<String> send({required String url, required ToCar data}) async {
    List<String> urls = url.split('/');
    // print(urls.join(","));
    String path = "";
    for (var i = 3; i < urls.length; i++) {
      path = path + "/" + urls[i];
    }
    switch (urls[0].startsWith("https")) {
      case true:
        uri = Uri.https(urls[2], path);
        break;
      case false:
        uri = Uri.http(urls[2], path);
        break;
    }
    Response res = await post(
      uri,
      body: jsonEncode(data),
      headers: {"Content-Type": "application/json"},
    );
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return "Error: " + res.body;
    }
  }
}
