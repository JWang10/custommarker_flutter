import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:routelines/controllers/mapController.dart';

class MapButtonBar extends StatelessWidget {
  const MapButtonBar({
    Key? key,
    required this.visible,
    required this.clearOnPressed,
    required this.navigatorOnPressed,
    required this.stopnavigatorOnPressed,
  })  : assert(visible != null),
        super(key: key);

  final bool visible;
  final Function clearOnPressed;
  final Function navigatorOnPressed;
  final Function stopnavigatorOnPressed;
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Container(
        padding: EdgeInsets.only(
          bottom: 14.0,
        ),
        alignment: Alignment.bottomLeft,
        child: GetX<MapController>(builder: (contorller) {
          return ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [
              Visibility(
                visible: !contorller.getNavStatus(),
                child: ElevatedButton(
                  style: buttonStyle(color: Colors.green[700]),
                  child: Text(
                    "Clear Waypoints",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                  onPressed: clearOnPressed as void Function()?,
                ),
              ),
              SizedBox(width: 5.0),
              Visibility(
                visible: contorller.markers.keys.contains(MarkerId("CAR")) &&
                        !contorller.getNavStatus() &&
                        visible
                    ? contorller.markers.length > 1
                    : false,
                child: ElevatedButton(
                  style: buttonStyle(color: Colors.blue[700]),
                  child: Text(
                    "Navigator",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: navigatorOnPressed as void Function()?,
                ),
              ),
              Visibility(
                visible: visible ? contorller.getNavStatus() : false,
                child: ElevatedButton(
                  style: buttonStyle(color: Colors.red[700]),
                  child: Text(
                    "STOP",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 40,
                    ),
                  ),
                  onPressed: stopnavigatorOnPressed as void Function()?,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }

  ButtonStyle buttonStyle({required Color? color}) {
    return ElevatedButton.styleFrom(
      primary: color,
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    );
  }
}
