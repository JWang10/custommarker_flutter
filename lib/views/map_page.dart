import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:routelines/controllers/mapController.dart';
import 'package:routelines/model/map_model.dart';
import '../components/map_button_bar.dart';
import '../components/map_dropdown_list.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;
const LatLng SOURCE_LOCATION = LatLng(25.033398, 121.552644);
const LatLng DEST_LOCATION = LatLng(25.033252, 121.557299);
const LatLng COMPANY_LOCATION = LatLng(25.0329048, 121.5549214);

class MapPage extends StatefulWidget {
  MapPage({Key? key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
// class MapPage extends GetView<MapController> {
  late BitmapDescriptor pinLocationIcon;
  late BitmapDescriptor carLocationIcon;
  late BitmapDescriptor sourceIcon;
  late BitmapDescriptor destinationIcon, destinationIcon1;
  late LatLng carLocation;
  Completer<GoogleMapController> _controller = Completer();
  // late GoogleMapController mapController;
  // this will hold each polyline coordinate as Lat and Lng pairs
  List<LatLng> polylineCoordinates = [];
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  // this is the key object - the PolylinePoints
  // which generates every polyline between start and finish
  PolylinePoints polylinePoints = PolylinePoints();

  String googleAPIKey = "AIzaSyDhyR7haB-DW8tpvUy2YH3UJuZajfMTRyY";

  int countPins = 0;
  int sumPins = 0;
  String dropdownValue = "";

  TextEditingController _pinTextController = TextEditingController();
  late Timer _timer;

  var _mapController = Get.find<MapController>();

  @override
  void initState() {
    super.initState();
    setCustomMapPin();
    setSourceAndDestinationIcons();

    /// Get Car data
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      _updateCarMarker();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
    _mapController.cancel();
    _pinTextController.dispose();
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'assets/pin.png');
    // .then((value) => pinLocationIcon = value);
    carLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 1), 'assets/car_128.png');
  }

  void setSourceAndDestinationIcons() async {
    sourceIcon = BitmapDescriptor.defaultMarker;
    destinationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'assets/pin.png');
  }

  @override
  Widget build(BuildContext context) {
    CameraPosition initialLocation = CameraPosition(
      target: COMPANY_LOCATION,
      zoom: 16,
      bearing: 30,
    );
    return SafeArea(
      child: Scaffold(
        body: GetBuilder<MapController>(
          builder: (_mapController) {
            return Stack(
              children: [
                GoogleMap(
                  myLocationButtonEnabled: true,
                  compassEnabled: true,
                  tiltGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  zoomGesturesEnabled: true,
                  // markers: _markers,
                  // polylines: _polylines,
                  markers: Set<Marker>.of(markers.values),
                  polylines:
                      Set<Polyline>.of(_mapController.getPolylines()!.values),
                  mapType: MapType.normal,
                  initialCameraPosition: initialLocation,
                  onMapCreated: onMapCreated,
                  onLongPress: onLongPress,
                ),
                MapButtonBar(
                  visible: markers.length > 1 ? true : false,
                  clearOnPressed: () {
                    HelperFunction.showCloseAlertDialog(
                      context: context,
                      onPressed: () {
                        _mapController.removeAllMarker(markers);
                        _showSnackBar('Remove all markers');
                        // Get.snackbar("Clear", 'Remove all markers');
                        Get.back();
                      },
                    );
                  },
                  navigatorOnPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Choose a target"),
                          content: SizedBox(
                            height: 50,
                            child: Row(
                              children: [
                                Text("Destination"),
                                Spacer(),
                                Expanded(
                                  child: DropDownList(
                                    // WHY is this part not work ?!
                                    // need to pull onChanged() to outside, then get the value changed
                                    markers: markers,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          actions: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.green[500],
                                    ),
                                    child: Text('Yes'),
                                    onPressed: () {
                                      var target =
                                          _mapController.getDropDownValue();
                                      if (target != "") {
                                        // // Start Navigatoring to target
                                        // // Draw a line from current location to target
                                        // // and show real-time current marker
                                        // polylineCoordinates.clear();
                                        // polylineCoordinates.add(carLocation);
                                        // //Draw line
                                        // var targetPoint = markers.values.where(
                                        //     (key) =>
                                        //         key.infoWindow.title == target);
                                        // polylineCoordinates
                                        //     .add(targetPoint.first.position);
                                        // _addPolyline();
                                        // // disable button
                                        // _mapController.setNavStatus(true);
                                        // // _mapController.setDropDownValue("");
                                        _mapController.sendTargetToCAR();
                                        Get.back();
                                      } else {
                                        HelperFunction.showAlertDialog(
                                            context: context,
                                            message:
                                                "Need to choose one target.");
                                      }
                                    },
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  ElevatedButton(
                                    child: Text('No'),
                                    onPressed: () {
                                      // Navigator.of(context).pop();
                                      Get.back();
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  stopnavigatorOnPressed: () {
                    // appState.setNavStatus(false);
                    _mapController.setNavStatus(false);
                  },
                ),
                Row(
                  children: [
                    BackButton(
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    // test for querying Car data
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red[500],
                        padding: EdgeInsets.only(
                          left: 8.0,
                          right: 8.0,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      child: Text(
                        "Get Car data",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      onPressed: () async {
                        // query Car data
                        _updateCarMarker();
                      },
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    // controller.setMapStyle(Utils.mapStyles);
    _controller.complete(controller);
    setMapInit();
    // _addPolyline();
    // _getPolyline();
  }

  void onLongPress(LatLng point) {
    _mapController.updateCountPin();
    _addMarker(point, _mapController.getCountPins().toString(),
        BitmapDescriptor.defaultMarkerWithHue(100));
  }

  Future<void> _updateCarMarker() async {
    String response = "";
    // var mapController = Get.find<MapController>();
    try {
      _mapController.updateCARdata();
      carLocation = LatLng(
          double.parse(_mapController.carStatus.position!.latitude!),
          double.parse(_mapController.carStatus.position!.longitude!));
      // carLocation = COMPANY_LOCATION;
      _addMarker(
        carLocation,
        "CAR",
        carLocationIcon,
      );

      // await mapController.animateCamera(
      //   // CameraUpdate.newLatLngBounds(
      //   //   LatLngBounds(
      //   //     southwest: LatLng(minLat, minLong),
      //   //     northeast: LatLng(maxLat, maxLong),
      //   //   ),
      //   //   48.0,
      //   // ),
      var cameraPosion = CameraPosition(
        target: carLocation,
        zoom: 16,
        bearing: 30,
      );
      //   CameraUpdate.newLatLng(carLocation),
      // );

    } catch (ex) {
      response = ex.toString();
      print(ex);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: Duration(seconds: 1),
          content: Text(response, style: TextStyle(fontSize: 16.0)),
        ),
      );
    }
  }

  void setMapInit() {
    setState(() {
      // source pin
      _addMarker(SOURCE_LOCATION, "Origin", sourceIcon);
      // destination pin
      _addMarker(DEST_LOCATION, "Destination", destinationIcon);
    });
  }

  void _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    MarkerId markerId = MarkerId(id);
    Marker marker = Marker(
      markerId: markerId,
      icon: descriptor,
      position: position,
      infoWindow: InfoWindow(
        title: id,
        snippet: "location: " +
            position.latitude.toStringAsPrecision(9) +
            "," +
            position.longitude.toStringAsPrecision(9),
        onTap: () => _pinDisplayTextInputDialog(
          context: context,
          markerId: markerId,
          title: "Point name",
          description: "name",
        ),
      ),
    );
    markers[markerId] = marker;
    _mapController.addMarker(markers);
  }

  // void _addPolyline() async {
  //   PolylineId id = PolylineId("poly");
  //   Polyline polyline = Polyline(
  //     polylineId: id,
  //     color: Colors.red,
  //     points: polylineCoordinates,
  //     // points: Provider.of<AppState>(context, listen: false).getPolylines(),
  //   );
  //   polylines[id] = polyline;
  //   _mapController.setPolylines(polylines);
  // }

  // void _getPolyline() async {
  //   PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
  //     googleAPIKey,
  //     PointLatLng(SOURCE_LOCATION.latitude, SOURCE_LOCATION.longitude),
  //     PointLatLng(DEST_LOCATION.latitude, DEST_LOCATION.longitude),
  //     travelMode: TravelMode.walking,
  //     // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
  //   );
  //   print(result.points);
  //   print(result.status);
  //   if (result.points.isNotEmpty) {
  //     result.points.forEach((PointLatLng point) {
  //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
  //     });
  //   } else {
  //     print(result.errorMessage);
  //   }
  //   _addPolyline();
  // }

  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 1),
        content: Text(message, style: TextStyle(fontSize: 16.0)),
      ),
    );
  }

  Future<void> _pinDisplayTextInputDialog(
      {required BuildContext context,
      required MarkerId markerId,
      required String title,
      required String description}) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: TextField(
            onChanged: (value) {},
            controller: _pinTextController,
            decoration: InputDecoration(
              hintText: description,
            ),
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red[500],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                      ),
                    ),
                    child: Text('Delete'),
                    onPressed: () {
                      _mapController.removeMarker(markerId);
                      Get.back();
                    },
                  ),
                  SizedBox(
                    child: Container(
                      margin: EdgeInsets.only(right: 50.0),
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green[500],
                    ),
                    child: Text('Save'),
                    onPressed: () {
                      // modify marker's name
                      _mapController.updateMarker(
                          markerId, _pinTextController.text);
                      _pinTextController.clear();
                      Get.back();
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    child: Text('Close'),
                    onPressed: () {
                      _pinTextController.clear();
                      // Navigator.of(context).pop();
                      Get.back();
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
