import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:get/get.dart';
import 'package:routelines/controllers/controllController.dart';

class ControlPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
      body: SafeArea(
        child: GetX<ControllController>(
          builder: (controller) {
            return Stack(
              children: [
                Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: BackButton(
                        onPressed: () {
                          // Navigator.of(context).pop();
                          Get.back();
                        },
                      ),
                    ),
                    Row(
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            controller.setMode("follow");
                          },
                          child: Text(
                            'Follow mode',
                            style: TextStyle(color: Colors.white, fontSize: 24),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Spacer(),
                        ElevatedButton(
                          onPressed: () {
                            controller.setMode("control");
                          },
                          child: Text(
                            'Control mode',
                            style: TextStyle(color: Colors.white, fontSize: 24),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Visibility(
                  visible: controller.getMode() == "control" ? true : false,
                  child: Container(
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: ClipOval(
                            child: Container(
                              color: Colors.blue[100],
                              height: 300.0,
                              width: 300.0,
                              child: null,
                            ),
                          ),
                        ),
                        DraggableCard(
                          // child: FlutterLogo(
                          //   size: 128,
                          // ),
                          child: ClipOval(
                            child: Container(
                              color: Colors.lightBlue,
                              height: 100.0,
                              width: 100.0,
                              child: Center(
                                child: Text(
                                  '+',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: controller.getMode() == "follow" ? true : false,
                  child: Center(
                    child: UnconstrainedBox(
                      child: Column(
                        children: [
                          Center(
                            child: Text(
                              "${controller.getMode().toUpperCase()}",
                              style: TextStyle(
                                  fontSize: 50, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Center(
                            child: Text(
                              "MODE",
                              style: TextStyle(
                                  fontSize: 50, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

/// A draggable card that moves back to [Alignment.center] when it's
/// released.
class DraggableCard extends GetView<ControllController> {
// class DraggableCard extends StatelessWidget {
  DraggableCard({this.child});
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Obx(
      () => GestureDetector(
        onPanDown: (details) {
          controller.animationController!.stop();
        },
        onPanUpdate: (details) {
          controller.setDragAlignment(details, size);
        },
        onPanEnd: (details) {
          controller.runAnimation(details.velocity.pixelsPerSecond, size);
        },
        child: Align(
          alignment: controller.dragAlignment,
          child: child,
        ),
      ),
    );
  }
}
