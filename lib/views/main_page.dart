import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:routelines/components/http_service.dart';
import 'package:routelines/controllers/mainController.dart';
import 'package:routelines/main.dart';

import '../constants.dart';

class MainPage extends StatelessWidget {
  var mainController = Get.find<MainController>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 16.0),
                    child: Obx(() => Text(
                          mainController.connectionStatus == false
                              ? Constant.mainConnectionStatus[0]
                              : Constant.mainConnectionStatus[1],
                          style: TextStyle(
                              color: mainController.connectionStatus == false
                                  ? Colors.red
                                  : Colors.green,
                              fontSize: 24),
                        )),
                  ),
                  IconButton(
                      icon: Icon(Icons.settings),
                      color: Colors.blue[400],
                      iconSize: 32,
                      onPressed: () {
                        Get.defaultDialog(
                          title: "Setting",
                          content: Padding(
                            padding: EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Obx(
                                  () => TextField(
                                    controller: mainController.tfController,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      errorBorder: OutlineInputBorder(),
                                      labelText: 'URL',
                                      errorText: mainController.tfValidate
                                          ? null
                                          : "value can\'t be empty or wrong format",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          confirm: TextButton(
                            style: TextButton.styleFrom(
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 8),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.green,
                                      width: 2,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(100)),
                            ),
                            onPressed: () async {
                              mainController.tfIsEmpty
                                  ? mainController.tfValidate = false
                                  : mainController.tfValidate = true;
                              if (!mainController.tfIsEmpty) {
                                try {
                                  mainController.tfValidate = true;
                                  await API.getCarData(
                                      url: mainController.tfController.text);
                                  mainController.connectionStatus = true;
                                  Get.back();
                                } catch (e) {
                                  printError(info: e.toString());
                                  mainController.tfValidate = false;
                                }
                              }
                            },
                            child: Text(
                              Constant.mainConnectionButton,
                              style: TextStyle(color: Colors.green),
                            ),
                          ),
                          cancel: TextButton(
                            style: TextButton.styleFrom(
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 8),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.blue,
                                      width: 2,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(100)),
                            ),
                            onPressed: () async {
                              Get.back();
                            },
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        );
                      }),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: size.height / 8),
                child: Image.asset("assets/logo.png"),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  Constant.maintitle,
                  style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 20),
              Obx(() => Visibility(
                    visible: mainController.connectionStatus ? true : false,
                    child: Column(
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Get.toNamed("/controller");
                          },
                          child: Text(
                            Constant.mainButtonController,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          style: buttonStyle,
                        ),
                        SizedBox(height: 20),
                        ElevatedButton(
                          onPressed: () {
                            Get.toNamed(Routes.MAP);
                          },
                          child: Text(
                            Constant.mainButtonNavigation,
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          style: buttonStyle,
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

ButtonStyle buttonStyle = ElevatedButton.styleFrom(
  shadowColor: Colors.black,
  elevation: 4.0,
  minimumSize: Size(100, 100),
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(35.0)),
  alignment: Alignment.center,
);
